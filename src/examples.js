import { getNextDate } from './helpers/DateHelper';

const examples = [
    {
        title: 'Time until Christmas',
        date: getNextDate(25, 'December'),
        format: ['months', 'days', 'hours', 'minutes'],
        thumbnail: '/img/examples/countdown-christmas.png',
        imageID: '4834888'
    },
    {
        title: 'Time until New Year',
        date: getNextDate(1, 'January'),
        format: ['days', 'minutes', 'hours', 'seconds'],
        thumbnail: '/img/examples/countdown-newyear.png',
        imageID: '2526105'
    },
    {
        title: 'Days until Birthday',
        date: getNextDate(14, 'April'),
        format: ['days'],
        thumbnail: '/img/examples/countdown-birthday.png',
        imageID: '769525'
    },
    {
        title: 'Days until Japan holiday',
        date: getNextDate(12, 'August'),
        format: ['days', 'hours', 'minutes', 'seconds'],
        thumbnail: '/img/examples/countdown-japan.png',
        imageID: '2235302'
    }
]


export default examples;