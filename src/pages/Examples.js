import { Component, Fragment} from 'react';
import Header from '../components/Header';
import Divider from '../components/Divider';
import Credits from '../components/Credits';
import { Link } from 'react-router-dom';
import { Container, Heading, Hero, Section, Columns } from 'react-bulma-components';
import generateCountdownURL from '../helpers/generateCountdownURL';
import examples from '../examples';



class Examples extends Component {
    state = { examples }


    renderExamples = () => {
        return this.state.examples.map( example =>  {
            const { title, date, format, thumbnail, imageID } = example;

            return (
                <Columns.Column size={6} >
                    <Link to={ generateCountdownURL(title, date, format, imageID) } target='_blank' >
                        <img src={ thumbnail } />
                        <p>{example.title}</p>
                    </Link>
                </Columns.Column>
            );
        });
    }

    
    render() {
        return (
            <Fragment>
                <Header/>

                <Hero size='medium'>
                    <Hero.Body>
                        <Container>
                            <Heading renderAs='h1'>Examples of Countdowns</Heading>
                            <Heading subtitle size={3}>Look through the examples below for inspiration.</Heading>
                        </Container>
                    </Hero.Body>
                </Hero>

                <Divider />

                <Section>
                    <Container >
                        <Columns>
                            { this.renderExamples() }
                        </Columns>
                    </Container>
                </Section>

                <Credits />
            </Fragment>
        );
    }
}



export default Examples;