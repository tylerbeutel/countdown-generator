import React, { Component, Fragment } from 'react';
import { Hero, Container, Heading, Footer, Content } from 'react-bulma-components';
import Credits from '../components/Credits';
import Clock from '../components/Clock';
import { getImage } from '../helpers/Pexels';
import { Link } from 'react-router-dom';



/* Responsible for getting values from URL and passing them to the Clock */
class Countdown extends Component {
    state = {
        image: {},
        title: '',
        desiredDate: null,
        format: {
            year: 0,
            month: 0,
            week: 0,
            hour: 0,
            minutes: 0
        }
    }

    
    componentDidMount = async () => {
        // Extract data from URL
        const search = this.props.location.search;
        const title = decodeURI( new URLSearchParams(search).get('title') );
        const desiredDate = decodeURI( new URLSearchParams(search).get('time') );
        const format = new URLSearchParams(search).get('format').replace('|', ',').split(',');
        this.setState({ format, title, desiredDate });

        // Download image for background
        const imageID =  new URLSearchParams(search).get('imageID');
        const image = await getImage(imageID);
        this.setState({ image });

        // Set title
        document.title = `${title} | Countdown Generator`;
    }


    renderCredits = () => {
        const { photographer_url, photographer, url } = this.state.image;

        return (
            <Credits>
                <p><Link to='/' target='_blank'>Build your own</Link> | Credits to <a target='_blank' href={photographer_url} >{photographer}</a> for their <a target='_blank' href={url}>image from Pexels</a> | Coded by <a target='_blank' href='https://tylerbeutel.com'>Tyler Beutel</a></p>
            </Credits>
        );
    }


    render() {
        return (
            <Fragment>
                <Hero size="fullheight" style={{
                    background: `url("${this.state.image.src ? this.state.image.src.large2x : ''}") no-repeat scroll center center/cover lightblue`
                }} >
                    <Hero.Body>
                        <Container style={{ 
                            backgroundColor: 'rgba(0,0,0,0.4)',
                            padding: 20,
                            borderRadius: 10
                        }}>
                            
                            
                            <Heading size={1} style={{ 
                                color: 'white', 
                                textAlign: 'center'
                            }}>{this.state.title}</Heading>

                            { this.state.desiredDate && <Clock 
                                desiredDate={ this.state.desiredDate } 
                                format={ this.state.format } 
                            /> }

                        </Container>
                    </Hero.Body>
                </Hero>

                { this.state.image && this.renderCredits() }
            </Fragment>
        );
    }
}



export default Countdown;