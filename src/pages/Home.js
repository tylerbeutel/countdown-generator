import { Component } from 'react';
import Header from '../components/Header';
import Divider from '../components/Divider';
import Credits from '../components/Credits';
import PexelsImageSelector from '../components/PexelsImageSelector';
import { Section, Container, Heading, Form, Hero, Columns, Button } from 'react-bulma-components';
import { Link } from 'react-router-dom';
import Datetime from 'react-datetime';
import generateCountdownURL from '../helpers/generateCountdownURL';
import "react-datetime/css/react-datetime.css";
const { Input, Field, Control, Label, Checkbox } = Form;



class Home extends Component {
    state = {
        title: '',
        desiredDate: new Date().toISOString(),
        imageID: '',
        durations: {
            years: true,
            months: true,
            days: true,
            hours: true,
            minutes: true,
            seconds: true
        }
    };


    getCountdownURL = () => {
        const { title, desiredDate, imageID } = this.state;

        // extract true format values from state
        let format = [];
        Object.entries(this.state.durations).map( duration => {
            if (duration[1] == true) {
                format.push(duration[0]);
            }
        });

        return generateCountdownURL(title, desiredDate, format, imageID);
    }


    onChange = (event) => {
        const value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
        this.setState({
          [event.target.name]: value,
        });
    }


    onImageSelect = (imageID) => {
        this.setState({ imageID });
    }

    
    onDurationChange = (event) => {
        this.setState({ 
            durations: {
                ...this.state.durations, 
                [event.target.name]: !this.state.durations[event.target.name] 
            } 
        });
    }


    render() {
        return (<>
            <Header/>

            <Hero size='medium'>
                <Hero.Body>
                    <Container>
                        <Heading renderAs='h1'>Build your own online website countdown</Heading>
                        <Heading subtitle size={3}>Choose your own title, background image and date.</Heading>
                    </Container>
                </Hero.Body>
            </Hero>

            <Divider />


            <Section>
                <Container >
                    {/* <Columns> */}
                        {/* <Columns.Column size={6} > */}
                            {/* Title */}
                            <Field>
                                <Label>Step 1 - Add Your Title:</Label>
                                <Control>
                                    <Input onChange={this.onChange} name="title" type="text" placeholder="Time until..." value={this.state.title} />
                                </Control>
                            </Field>
                        {/* </Columns.Column> */}

                        {/* <Columns.Column size={6} > */}
                            {/* Time */}
                            <Field style={{ maxWidth: 500 }}>
                                <Label>Step 2 - Add Your Target Date:</Label>
                                <Control>
                                    <Datetime
                                        onChange={ (e) => this.setState({ desiredDate: e._d.toISOString() }) }
                                        input={false}
                                        inputProps={{
                                            className: 'input',
                                            disabeled: true,
                                            placeholder: 'DD/MM/YYYY hh:mm AM',
                                            value: this.state.desiredDate,
                                    }} />
                                </Control>
                            </Field>
                        {/* </Columns.Column> */}
                    {/* </Columns> */}


                    {/* Durations */}
                    <Field style={{ marginBottom: 50 }}>
                        <Label>Step 3: Choose Which Durations You Want Displayed:</Label>
                        <Control><Checkbox onChange={this.onDurationChange} name="years" checked={this.state.durations.years} /> Years</Control>
                        <Control><Checkbox onChange={this.onDurationChange} name="months" checked={this.state.durations.months} /> Months</Control>
                        <Control><Checkbox onChange={this.onDurationChange} name="days" checked={this.state.durations.days} /> Days</Control>
                        <Control><Checkbox onChange={this.onDurationChange} name="hours" checked={this.state.durations.hours} /> Hours</Control>
                        <Control><Checkbox onChange={this.onDurationChange} name="minutes" checked={this.state.durations.minutes} /> Minutes</Control>
                        <Control><Checkbox onChange={this.onDurationChange} name="seconds" checked={this.state.durations.seconds} /> Seconds</Control>
                    </Field>


                    {/* Background Image */}
                    <PexelsImageSelector onImageSelect={ this.onImageSelect } />


                </Container>
            </Section>

            <Section>
                <Container>
                    {/* <Notification color="primary">
                        <Heading>Output</Heading>
                        <Link to={ this.getCountdownURL }><Heading subtitle>{ this.getCountdownURL() }</Heading></Link>
                    </Notification> */}
                    <Link target="_blank" to={ this.getCountdownURL() } >
                        <Button color="primary" size="medium" >Start Countdown!</Button>
                    </Link>
                </Container>
            </Section>

            <Credits/>
        </>);
    }
}



export default Home;