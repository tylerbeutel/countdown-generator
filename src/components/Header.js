import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Container, Navbar } from 'react-bulma-components';



const Header = () => {
    const [hamburgerMenuActive, setHamburgerMenu] = useState(false);
    const [menuDisplayValue, setMenuDisplayValue] = useState(false);


    useEffect(() => {
        // Show menu if hamburger is active OR if viewed from desktop.
        setMenuDisplayValue( (hamburgerMenuActive || window.innerWidth > 1023) ? true : false );
    });


    return (
        <Navbar fixed='top' style={{ borderBottom: '1px solid #f5f5f5' }}>
            <Container>
                <Navbar.Brand>
                    <Navbar.Item>
                        <Link to='/' >
                            <img src='/img/brand/logo.png' style={{ maxHeight: '3rem', marginTop: 5 }} />
                        </Link>
                    </Navbar.Item>
                    <Navbar.Burger 
                        onClick={() => setHamburgerMenu(!hamburgerMenuActive)} 
                        className={hamburgerMenuActive == true ? 'is-active' : ''}
                        style={{ width: '4.75rem', height: '4.75rem' }}
                    />
                </Navbar.Brand>
                
                <Navbar.Container position="end">
                    <Navbar.Menu className={menuDisplayValue == true ? 'menu-visible' : ''} >
                        <Navbar.Item>
                            <Link to='/' style={{ color: '#363636' }} >Build Your Own</Link>
                        </Navbar.Item>
                        <Navbar.Item>
                            <Link to='/examples/' style={{ color: '#363636' }} >See Examples</Link>
                        </Navbar.Item>
                    </Navbar.Menu>
                </Navbar.Container>
            </Container>
        </Navbar>
    );
}



export default Header;