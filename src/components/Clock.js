import { Component } from 'react';
import { Container, Columns } from 'react-bulma-components';
import ClockCard from './ClockCard';
import { DateTime } from "luxon";
import { timeLeft } from '../helpers/DateHelper';



class Clock extends Component {

    componentDidMount = () => {
        // Rerender view every 1 second
        this.interval = setInterval(() => this.setState({ time: Date.now() }), 1000);
    }

    
    componentWillUnmount() {
        clearInterval(this.interval);
    }


    renderCards = () => {
        const { desiredDate, format } = this.props;

        const endDate = DateTime.fromISO( desiredDate, {setZone: true});
        const count = timeLeft( endDate, format );

        return Object.keys(count).map( key => <ClockCard label={key} value={Math.floor(count[key]) } />)
    }


    render () {
        return (
            <Container>
                <Columns>
                    { this.renderCards() }
                </Columns>
            </Container>
        );
    }
}

    

export default Clock;