import React, { Component } from 'react';
import { Container, Columns, Form } from 'react-bulma-components';
import { searchPexels } from '../helpers/Pexels';
const { Input, Field, Control, Label } = Form;



class PexelsImageSelector extends Component {
    state = {
        query: 'Japan',
        images: [],
        selectedImageID: ''
    };


    componentDidMount = () => {
        this.searchPexels();
    }


    searchPexels = async () => {
        if (this.state.query) {
            let images = await searchPexels(this.state.query);
            this.setState({ images });
        }
    }


    onQueryChange = (event) => {
        this.setState({ query: event.target.value }, 
            () => this.searchPexels()
        );
    }


    onImageSelect = (event) => {
        const selectedImageID = event.target.attributes.imageID.value;

        this.props.onImageSelect(selectedImageID);
        this.setState({ selectedImageID });

        console.log(event.target.attributes.imageID.value);
    }


    renderResults = () => {
        let images = this.state.images;

        if (images) {
            return images.map( image => {
                return (
                    <Columns.Column size={3} >
                        <div 
                            imageID={ image.id }
                            onClick={ this.onImageSelect } 
                            style={{
                                background: `url('${image.src.medium}') no-repeat scroll center center/cover lightblue`,
                                backgroundSize: 'cover',
                                borderRadius: 5,
                                width: '100%',
                                height: 200,
                                border: (image.id == this.state.selectedImageID ? '5px solid #00d1b2' : 'none')
                            }} 
                        />
                    </Columns.Column>
                );
            });
        }

        return <p>loading...</p>;
    }


    render() {
        return (
            <Container>
                <Field>                    
                    <Label>Step 4 - Choose A Background Image:</Label>
                    <Control>
                        <Input
                            value={ this.state.query }
                            onChange={ this.onQueryChange }
                            placeholder='Image search...'
                        />
                    </Control>
                </Field>
                
                <Columns>
                    { this.renderResults() }
                </Columns>
            </Container>
        );
    }
}



export default PexelsImageSelector;