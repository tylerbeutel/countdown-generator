import React from 'react';
import { Footer, Container, Content } from 'react-bulma-components';


const Credits = ({ children }) => {
    return (
        <Footer style={{ padding: 16 }}>
            <Container>
                <Content style={{ textAlign: 'center' }}>
                    { children ? children : <p>Coded by <a target='_blank' href='https://tylerbeutel.com'>Tyler Beutel</a> | © tylerbeutel.com</p> }
                </Content>
            </Container>
        </Footer>
    );
}


export default Credits;