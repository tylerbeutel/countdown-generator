import React from 'react';
import { Columns, Notification, Heading } from 'react-bulma-components';


const ClockCard = ({ label, value }) => {
    return (
        <Columns.Column>
            <Notification style={{ textAlign: 'center' }}>
                <Heading size={1} >{value}</Heading>
                <Heading size={5} subtitle >{label}</Heading>
            </Notification>
        </Columns.Column>
    );
}


export default ClockCard;