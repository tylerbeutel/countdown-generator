import React from 'react';
import { Section, Container } from 'react-bulma-components';


const Divider = () => {
    return (
        <Section style={{ padding: 0 }}>
            <Container style={{ borderTop: "2px solid #f5f5f5" }} />
        </Section>
    );
}


export default Divider;