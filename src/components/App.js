// Dependencies
import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import Home from '../pages/Home';
import Countdown from '../pages/Countdown';
import Examples from '../pages/Examples';


const App = () =>
    <BrowserRouter basename='/' >
        <Route exact path='/' component={Home} />
        <Route exact path='/examples/' component={Examples} />
        <Route exact path='/c' component={Countdown} />
    </BrowserRouter>


export default App;