import { DateTime, Interval } from "luxon";


/**
 * Determines whether a specified date has occured.
 */
export const hasHappened = (desiredDate) => {
    /* Define dates */
    let start = DateTime.local();
    const end = DateTime.fromISO( desiredDate );

    /* Find difference */
    return start >= end;
}


/**
 * Gets the next available specified date.
 */
export const getNextDate = (day, month) => {
    let currentDate = DateTime.local();
    let desiredDate = DateTime.fromFormat(`${day} ${month} ${currentDate.year}`, "d LLLL yyyy");

    if (currentDate > desiredDate) {
        return DateTime.fromFormat(`${day} ${month} ${currentDate.year+1}`, "d LLLL yyyy").toISO({ includeOffset: false });
    }

    return desiredDate.toISO({ includeOffset: false });
}



/** 
 * Tells you how long until an desired datetime.
 */
export const timeLeft = ( desiredDate, format ) => {

    // default format
    let returnFormat = ['years', 'months', 'days', 'hours', 'minutes', 'seconds'];
    // override format
    if (format) {
        returnFormat = format;
    }
 
    /* Define dates */
    let start = DateTime.local();
    const end = desiredDate;

    /* Find difference */
    let i = Interval.fromDateTimes(start, end);
    let count = i.toDuration( format ).toObject();

    /* Set all date values to zero if duration failed */
    if ( Object.keys(count).length == 0 ) {
        count = {};
        format.map(duration => { count[duration] = 0; });
    }

    /* Render Cards */
    return count;
}

