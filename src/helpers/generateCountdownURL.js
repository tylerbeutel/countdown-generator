/**
 * Generates a relative link incorperating the provided data.
 */
const generateCountdownURL = (title, desiredDate, format, imageID) => {
    return `/c?title=${encodeURI(title)}&time=${desiredDate}&format=${format.join(',')}&imageID=${imageID}`;
}

export default generateCountdownURL;