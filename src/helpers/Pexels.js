/**
 * Header configuration ready for the methods to use.
 */
const headerConfig = {
    method: 'GET',
    headers: { Authorization: '563492ad6f91700001000001b94b0217d5c2478192ad25cdf8032357' }
};



/**
 * Searches Pexels with specified query string.
 */
export const searchPexels = async (query) => {
    let response = await fetch(`https://api.pexels.com/v1/search?per_page=16&query=${query}`, headerConfig);
    let data = await response.json();

    return data.photos;
}



/**
 * Gets an image from Pexels that matches the specified ID.
 */
export const getImage = async (imageID) => {
    let response = await fetch(`https://api.pexels.com/v1/photos/${imageID}`, headerConfig);
    let data = await response.json();
    
    return data;
}